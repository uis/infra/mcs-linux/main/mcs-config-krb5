mcs-config-krb5 (13build2) 2019; urgency=medium

  * No-change rebuild for MCS Linux 2019. 

 -- David McBride <dwm37@cam.ac.uk>  Fri, 09 Aug 2019 14:05:57 +0100

mcs-config-krb5 (13build1) 2018; urgency=medium

  * No-change rebuild for MCS Linux 2018. 

 -- David McBride <dwm37@cam.ac.uk>  Wed, 01 Aug 2018 11:01:09 +0100

mcs-config-krb5 (13) 2017; urgency=medium

  * smbspool apparently doesn't cope with credential caches being
    stored in kernel keyrings.  (It should, but something, somewhere,
    is wrong.) Revert this change until we can figure out what's wrong.

 -- David McBride <dwm37@cam.ac.uk>  Wed, 30 Aug 2017 20:51:30 +0100

mcs-config-krb5 (12) 2017; urgency=medium

  * Update for MCS Linux 2017.
  * Change krb5.conf configuration to store credential caches in
    persistent per-UID kernel keyrings rather than as files in /tmp.

    (This will have knock-on effects for any privileged code that
     directly searches ccache files in /tmp for keys.  This might
     include both the kernel CIFS filesystem driver and the custom
     local CUPS printer backend.)

 -- David McBride <dwm37@cam.ac.uk>  Fri, 09 Jun 2017 13:56:23 +0100

mcs-config-krb5 (11) 2016; urgency=medium

  * Update for MCS Linux 2016.
  * Remove references to non-existant DS.CAM.AC.UK Realm. 

 -- David McBride <dwm37@cam.ac.uk>  Mon, 05 Sep 2016 21:07:45 +0100

mcs-config-krb5 (10) 2016; urgency=medium

  * No-change rebuild for MCS Linux 2016.

 -- David McBride <dwm37@cam.ac.uk>  Wed, 10 Aug 2016 15:54:29 +0000

mcs-config-krb5 (9) 2015; urgency=medium

  * Add /etc/krb5.conf.mcs to list of files permitted to
    be read by Kerberos clients by processes contained using Apparmor.

 -- David McBride <dwm37@cam.ac.uk>  Mon, 05 Oct 2015 15:19:10 +0100

mcs-config-krb5 (8) 2015; urgency=medium

  * Update for MCS Linux 2015.
  * Stop hardcoding the DNS names of the PWFAD Kerberos Realm;
    fall back to DNS auto-discovery.
  * Update Uploaders and Maintainers fields.

 -- David McBride <dwm37@cam.ac.uk>  Mon, 11 May 2015 19:45:13 +0100

mcs-config-krb5 (7) 2014;

  * Add cdbs to Build-Depends.
  * Generall update debian/control.

 -- Ben Harris <bjh21@cam.ac.uk>  Thu, 22 May 2014 18:50:46 +0100

mcs-config-krb5 (6) 2013; urgency=low

  * No-change rebuild for 2013.

 -- David McBride <dwm37@cam.ac.uk>  Thu, 09 May 2013 16:22:49 +0100

mcs-config-krb5 (5) 2012; urgency=low

  * Setting default ticket lifetime also disabled renewable tickets so set
    the renewable_time as well to 1 week which is what the AD allows as a
    maximum.

 -- Anton Altaparmakov <aia21@cam.ac.uk>  Fri, 17 Aug 2012 10:30:00 +0100

mcs-config-krb5 (4) 2012; urgency=low

  * Set default ticket lifetime to 10 hours to be like Raven.

 -- Anton Altaparmakov <aia21@cam.ac.uk>  Mon, 13 Aug 2012 11:25:58 +0100

mcs-config-krb5 (2) oneiric-uxsup; urgency=low

  * Fix typo: "dc.cam" -> "ds.cam".

 -- Anton Altaparmakov <aia21@cam.ac.uk>  Thu, 5 Apr 2012 13:59:49 +0000

mcs-config-krb5 (1) oneiric-uxsup; urgency=low

  * Default Ubuntu configuration with University servers.

 -- Anton Altaparmakov <aia21@cam.ac.uk>  Wed, 21 Mar 2012 09:33:30 +0000
